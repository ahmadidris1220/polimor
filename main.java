package week9;

public class Main {
    public static void main(String[] args) {
        
        BangunDatar persegi = new BangunDatar(5);
        System.out.println("Luas persegi dengan sisi 5: " + persegi.luas(persegi.getSisi()));
        
        BangunDatar persegiPanjang = new BangunDatar(4, 6);
        System.out.println("Luas persegi panjang dengan panjang 4 dan lebar 6: " + persegiPanjang.luas(persegiPanjang.getPanjang(), persegiPanjang.getLebar()));
        
       
        BangunRuang balok = new BangunRuang(4, 6, 10);
        System.out.println("Volume balok dengan panjang 4, lebar 6, dan tinggi 10: " + balok.volume());
    }
}