public class BangunRuang extends BangunDatar
{
    private int tinggi;

    public BangunRuang(int panjang, int lebar, int tinggi)
    {
        super(panjang, lebar);  
        this.tinggi = tinggi;
    }

    public int volume()
    {
        
        return super.luas(panjang, lebar) * tinggi;  
    }
}