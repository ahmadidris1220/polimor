package week9;

public class BangunDatar
{
    private int panjang;
    private int lebar;
    private int diameter;
    private int sisi;
    private static final double pi = 3,14;

    public BangunDatar (int sisi)
    {
        this.sisi = sisi;
    }

    public BangunDatar (int panjang, int lebar)
    {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    public int luas (int a)
    {
        return a*a;
    }

    public int luas (int a, int b)
    {
        return a*b;
    }

    pubic int getSisi()
    {
        return sisi;
    }
}